// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright 2019 NXP
 */

#include <common.h>
#include <i2c.h>
#include <fdt_support.h>
#include <asm/io.h>
#include <asm/arch/clock.h>
#include <asm/arch/fsl_serdes.h>
#include <asm/arch/ppa.h>
#include <asm/arch/soc.h>
#include <asm/arch-fsl-layerscape/fsl_icid.h>
#include <hwconfig.h>
#include <ahci.h>
#include <mmc.h>
#include <scsi.h>
#include <fm_eth.h>
#include <fsl_csu.h>
#include <fsl_esdhc.h>
#include <power/mc34vr500_pmic.h>
#include <fsl_sec.h>
#include <fsl_dspi.h>

#define LS1046A_PORSR1_REG 0x1EE0000
#define BOOT_SRC_SD        0x20000000
#define BOOT_SRC_MASK	   0xFF800000
#define BOARD_REV_GPIO		13
#define USB2_SEL_MASK	   0x00000100
#define DEBUG_SEL_MASK	   0x00800000

#define	SD_EMMC_SEL_PIN   0x00040000       //GPIO3_13
#define RST_EMMC_B_PIN    0x00000020       //GPIO3_26
#define	SPI_EMMC_SEL_PIN  0x01000000       //GPIO3_07  
#define EVDD_EN_PIN       0x00004000       //GPIO3_17
#define EVDD_SEL_PIN      0x00008000       //GPIO3_16
#define SDHC_CD_B_PIN     0x20000000       //GPIO4_02
#define	EMMC_SEL_VAL	  1
#define SD_SEL_VAL        0
#define GPIOMASK	  0x80000000

#define BYTE_SWAP_32(word)  ((((word) & 0xff000000) >> 24) |  \
(((word) & 0x00ff0000) >>  8) | \
(((word) & 0x0000ff00) <<  8) | \
(((word) & 0x000000ff) << 24))
#define SPI_MCR_REG	0x2100000

DECLARE_GLOBAL_DATA_PTR;

int select_i2c_ch_pca9547(u8 ch)
{
	int ret;

	ret = i2c_write(I2C_MUX_PCA_ADDR_PRI, 0, 1, &ch, 1);
	if (ret) {
		puts("PCA: failed to select proper channel\n");
		return ret;
	}

	return 0;
}

static inline void demux_select_usb2(void)
{
	u32 val;
	struct ccsr_gpio *pgpio = (void *)(GPIO3_BASE_ADDR);

	val = in_be32(&pgpio->gpdir);
	val |=  USB2_SEL_MASK;
	out_be32(&pgpio->gpdir, val);

	val = in_be32(&pgpio->gpdat);
	val |=  USB2_SEL_MASK;
	out_be32(&pgpio->gpdat, val);
}

/* for debug select sd or emmc on PCU board  --wzh  20191231 */
u32 eMMC_SD_detection(void)
{
	u32 sd_emmc_val;
	struct ccsr_gpio *pgpio4 = (void *)(GPIO4_BASE_ADDR);
	sd_emmc_val = in_be32(&pgpio4->gpdat) & SDHC_CD_B_PIN;
	return(sd_emmc_val); 	
}

void demux_select_sd_emmc(void)
{
	u32 val;
	u32 sd_emmc_val;

/*	
	for (i = 0; i < ARRAY_SIZE(gur->rcwsr); i++) {
		rcw = gur_in32(&gur->rcwsr[i]);
		if ((i % 4) == 0)
			printf("\n       %08x:", i * 4);
		printf(" %08x", rcw);
	}
	puts("\n");
	
*/
	struct ccsr_gur __iomem *gur = (void *)(CONFIG_SYS_FSL_GUTS_ADDR);
	struct ccsr_gpio *pgpio3 = (void *)(GPIO3_BASE_ADDR);
	struct ccsr_gpio *pgpio4 = (void *)(GPIO4_BASE_ADDR);

// = gur_in32(&gur->rcwsr[13]);
//cw1 = gur_in32(&gur->rcwsr[10]);
	
	val = in_be32(&pgpio4->gpdir);
	val &= ~(SDHC_CD_B_PIN);
	out_be32(&pgpio4->gpdir,val);
	
	val = in_be32(&pgpio3->gpdir);
	val |= SD_EMMC_SEL_PIN;
	val |= SPI_EMMC_SEL_PIN;
	val |= RST_EMMC_B_PIN;
	val |= EVDD_EN_PIN;
	val |= EVDD_SEL_PIN;                  
	out_be32(&pgpio3->gpdir,val);

	sd_emmc_val = in_be32(&pgpio4->gpdat) & SDHC_CD_B_PIN;
	if(sd_emmc_val)
	{
		/* &= 0xFFFFFE7F;
		gur_out32(&gur->rcwsr[13],rcw);
		rcw1 &= 0xFF1FFFC;
		rcw1 |= 1<<21;
		rcw1 |= 2;
		gur_out32(&gur->rcwsr[10],rcw1);*/
		
		val = in_be32(&pgpio3->gpdat);
		val |= SD_EMMC_SEL_PIN;
		val |= SPI_EMMC_SEL_PIN;
		val |= EVDD_EN_PIN;
		val |= EVDD_SEL_PIN;
		out_be32(&pgpio3->gpdat,val);

		val = in_be32(&pgpio3->gpdat);
		val &= ~(RST_EMMC_B_PIN);
		out_be32(&pgpio3->gpdat,val);
		mdelay(15);
		val = in_be32(&pgpio3->gpdat);
		val |= RST_EMMC_B_PIN;
		out_be32(&pgpio3->gpdat,val);
		puts("eMMC channel is enable !\n");
		
	}
	else
	{
	/*cw &= 0xFFFFFE7F;
		rcw |= (2<<7);
		gur_out32(&gur->rcwsr[13],rcw);
		rcw1 &= 0xFF1FFFC;
		gur_out32(&gur->rcwsr[10],rcw1);*/
		
		val = in_be32(&pgpio3->gpdat);
		val &= ~SD_EMMC_SEL_PIN;
		val &= ~SPI_EMMC_SEL_PIN;
		val &= ~EVDD_SEL_PIN;
		val |=  EVDD_EN_PIN;
		out_be32(&pgpio3->gpdat,val);
		puts("SD channel is enable !\n");		
	}
}

/* for debug uart 20190725 */
static inline void demux_select_gpio(void)
{
	u32 val;
	struct ccsr_gpio *pgpio = (void *)(GPIO3_BASE_ADDR);

	val = in_be32(&pgpio->gpdir);
	val |=  DEBUG_SEL_MASK;
	val |= 0x20000; //gpio3_14 output serdes2 minipcie reset	
	val |= 0x300000; //gpio3_10 gpio3_11 high
	//val |= 0x440000; //gpio3_09 / gpio3_13 output for test
	out_be32(&pgpio->gpdir, val);

	//gpio3_14 output serdes2 for minipcie reset 
	val = in_be32(&pgpio->gpdat);
	val &= ~0x20000;
	out_be32(&pgpio->gpdat, val);
	mdelay(10);
	
	val = in_be32(&pgpio->gpdat);
	val |=  DEBUG_SEL_MASK;
	val |= 0x20000; //gpio3_14 output serdes2 minipcie reset
	val |= 0x300000; //gpio3_10 gpio3_11 high
	//val |= 0x400000; //gpio3_09:high gpio3_13:low
	out_be32(&pgpio->gpdat, val);

	//gpio3_14 output serdes2 for minipcie reset 
	mdelay(10);
	val = in_be32(&pgpio->gpdat);
	val &= ~0x20000;
	out_be32(&pgpio->gpdat, val);
}

static inline void set_spi_cs_signal_inactive(void)
{
	/* default: all CS signals inactive state is high */
	uint mcr_val;
	uint mcr_cfg_val = DSPI_MCR_MSTR | DSPI_MCR_PCSIS_MASK |
				DSPI_MCR_CRXF | DSPI_MCR_CTXF;

	mcr_val = in_be32(SPI_MCR_REG);
	mcr_val |= DSPI_MCR_HALT;
	out_be32(SPI_MCR_REG, mcr_val);
	out_be32(SPI_MCR_REG, mcr_cfg_val);
	mcr_val = in_be32(SPI_MCR_REG);
	mcr_val &= ~DSPI_MCR_HALT;
	out_be32(SPI_MCR_REG, mcr_val);
}

int board_early_init_f(void)
{
	fsl_lsch2_early_init_f();

	return 0;
}

#ifndef CONFIG_SPL_BUILD
static inline uint8_t get_board_version(void)
{
	u8 val;
	struct ccsr_gpio *pgpio = (void *)(GPIO2_BASE_ADDR);

	val = (in_le32(&pgpio->gpdat) >> BOARD_REV_GPIO) & 0x03;

	return val;
}

int checkboard(void)
{
	static const char *freq[2] = {"100.00MHZ", "100.00MHZ"};
	u32 boot_src;
	u8 rev;

	rev = get_board_version();
	switch (rev) {
	case 0x00:
		puts("Board: LS1046AFRWY, Rev: A, boot from ");
		break;
	case 0x01:
		puts("Board: LS1046AFRWY, Rev: B, boot from ");
		break;
	default:
		puts("Board: LS1046AFRWY, Rev: Unknown, boot from ");
		break;
	}
	boot_src = BYTE_SWAP_32(readl(LS1046A_PORSR1_REG));

	if ((boot_src & BOOT_SRC_MASK) == BOOT_SRC_SD)
		puts("SD\n");
	else
		puts("QSPI\n");
	printf("SD1_CLK1 = %s, SD1_CLK2 = %s\n", freq[0], freq[1]);

	return 0;
}

int board_init(void)
{
#ifdef CONFIG_SECURE_BOOT
	/*
	 * In case of Secure Boot, the IBR configures the SMMU
	 * to allow only Secure transactions.
	 * SMMU must be reset in bypass mode.
	 * Set the ClientPD bit and Clear the USFCFG Bit
	 */
	u32 val;
val = (in_le32(SMMU_SCR0) | SCR0_CLIENTPD_MASK) & ~(SCR0_USFCFG_MASK);
	out_le32(SMMU_SCR0, val);
	val = (in_le32(SMMU_NSCR0) | SCR0_CLIENTPD_MASK) & ~(SCR0_USFCFG_MASK);
	out_le32(SMMU_NSCR0, val);
#endif

#ifdef CONFIG_FSL_CAAM
	sec_init();
#endif

#ifdef CONFIG_FSL_LS_PPA
	ppa_init();
#endif
	select_i2c_ch_pca9547(I2C_MUX_CH_DEFAULT);
	demux_select_sd_emmc();  // Add for switch sd and emmc pin on PCU board 20191231
	return 0;
}

int board_setup_core_volt(u32 vdd)
{
	return 0;
}

int get_serdes_volt(void)
{
	return mc34vr500_get_sw_volt(SW4);
}

int set_serdes_volt(int svdd)
{
	return mc34vr500_set_sw_volt(SW4, svdd);
}

int power_init_board(void)
{
	int ret;

	ret = power_mc34vr500_init(0);
	if (ret)
		return ret;

	setup_chip_volt();
	return 0;
}

void config_board_mux(void)
{
#ifdef CONFIG_HAS_FSL_XHCI_USB
	struct ccsr_scfg *scfg = (struct ccsr_scfg *)CONFIG_SYS_FSL_SCFG_ADDR;
	u32 usb_pwrfault;
	/*
	 * USB2 is used, configure mux to USB2_DRVVBUS/USB2_PWRFAULT
	 * USB3 is not used, configure mux to IIC4_SCL/IIC4_SDA
	 */
	out_be32(&scfg->rcwpmuxcr0, 0x3300);
#ifdef CONFIG_HAS_FSL_IIC3
	/* IIC3 is used, configure mux to use IIC3_SCL/IIC3/SDA */
	out_be32(&scfg->rcwpmuxcr0, 0x0000);
#endif
	out_be32(&scfg->usbdrvvbus_selcr, SCFG_USBDRVVBUS_SELCR_USB1);
	usb_pwrfault = (SCFG_USBPWRFAULT_DEDICATED <<
			SCFG_USBPWRFAULT_USB3_SHIFT) |
			(SCFG_USBPWRFAULT_DEDICATED <<
			SCFG_USBPWRFAULT_USB2_SHIFT) |
			(SCFG_USBPWRFAULT_SHARED <<
			SCFG_USBPWRFAULT_USB1_SHIFT);
	out_be32(&scfg->usbpwrfault_selcr, usb_pwrfault);
#ifndef CONFIG_HAS_FSL_IIC3
	/*
	 * LS1046A FRWY board has demultiplexer NX3DV42GU with GPIO3_23 as input
	 * to select I2C3_USB2_SEL_IO
	 * I2C3_USB2_SEL = 0: I2C3_SCL/SDA signals are routed to
	 * I2C3 header (default)
	 * I2C3_USB2_SEL = 1: USB2_DRVVBUS/PWRFAULT signals are routed to
	 * USB2 port
	 * programmed to select USB2 by setting GPIO3_23 output to one
	 */
	demux_select_usb2();
#endif
#endif
	demux_select_gpio(); //Add for debug uart on PCU board 20190723
	set_spi_cs_signal_inactive();
}

#ifdef CONFIG_MISC_INIT_R
int misc_init_r(void)
{
	config_board_mux();
	return 0;
}
#endif

int ft_board_setup(void *blob, bd_t *bd)
{
	u64 base[CONFIG_NR_DRAM_BANKS];
	u64 size[CONFIG_NR_DRAM_BANKS];

	/* fixup DT for the two DDR banks */
	base[0] = gd->bd->bi_dram[0].start;
	size[0] = gd->bd->bi_dram[0].size;
	base[1] = gd->bd->bi_dram[1].start;
	size[1] = gd->bd->bi_dram[1].size;

	fdt_fixup_memory_banks(blob, base, size, 2);
	ft_cpu_setup(blob, bd);

#ifdef CONFIG_SYS_DPAA_FMAN
	fdt_fixup_fman_ethernet(blob);
#endif

	fdt_fixup_icid(blob);

	return 0;
}
#endif
